#!/usr/bin/env bash

export path=$(readlink -f $(dirname $0) )

if [ -z $1 ]; then echo "You have to provide an API token as first argument!" && exit 1; fi

echo "s/'${OWM_TOKEN}'/${1}/g"
sed s/\$\{OWM_TOKEN\}/${1}/g -i $path/weather.yaml
